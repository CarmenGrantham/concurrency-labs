package lab6;

import java.math.BigInteger;

public class Permutation {

    public static void main(String[] args) {
        BigInteger p1 = permutation(BigInteger.valueOf(21), BigInteger.valueOf(7));
        System.out.println("permutation(21,7) = " + p1);
        BigInteger p2 = permutation(BigInteger.valueOf(14), BigInteger.valueOf(7));
        System.out.println("permutation(14,7) = " + p2);
        BigInteger p3 = permutation(BigInteger.valueOf(7), BigInteger.valueOf(7));
        System.out.println("permutation(7,7) = " + p3);
        
    }
    
    private static BigInteger permutation(BigInteger n, BigInteger k) {
        BigInteger result = BigInteger.ONE;
        
        while (n.compareTo(k) >= 0 && k.compareTo(BigInteger.ZERO) > 0) {
            result = result.multiply(n);
            n = n.subtract(BigInteger.ONE);
            k = k.subtract(BigInteger.ONE);
        }
        
        return result;
    }
}
