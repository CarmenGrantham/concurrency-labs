package lab6;

import java.math.BigInteger;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ThreeThreadedPermutations {
    public static void main(String[] args) {
        Permutation perm1 = new Permutation(21, 7);
        System.out.println("Result of (21,7) is " + perm1.call());
        
        // Checking for performance
        Permutation perm2 = new Permutation(21000, 21000);
        BigInteger seqfact = BigInteger.ONE;
        BigInteger parfact = BigInteger.ONE;
        // Start the clock
        long start = System.nanoTime();
        // Do sequential factorial
        for (int i = 1; i < 10; i++) {
            seqfact = perm2.call();
            System.out.print(".");
        }
        System.out.println();
        
        // Lap time
        long mid = System.nanoTime();
        // The parallel factorial
        for (int i = 1; i < 10; i++) {
            try {
                parfact = parfactorial();
                System.out.print(".");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        System.out.println();
        
        // Stop the clock
        long end = System.nanoTime();
        
        // Print out elapsed time
        System.out.printf("Single threaded took %.3f sec, multi-thread took %.3f%n", (mid-start) / 1e9, (end - mid) / 1e9);
        
        // Print out results
        System.out.println("seq factorial is " + seqfact);
        System.out.println("\n\npar factorial is " + parfact);
        
    }
    

    public static class Permutation implements Callable<BigInteger> {
        private long n;
        private long k;
        
        public Permutation(long n, long k) {
            this.n = n;
            this.k = k;
        }
        
        public BigInteger call() {
            BigInteger result = BigInteger.ONE;
            
            long a = n;
            long b = k;
            
            while (a >= b && b > 0) {
                result = result.multiply(BigInteger.valueOf(a));
                a--;
                b--;
            }
            
            return result;
        }
    }
    
    public static BigInteger parfactorial() throws ExecutionException, InterruptedException {
        BigInteger finalResult = BigInteger.ONE;
        
        try {
            ExecutorService service1 = Executors.newSingleThreadExecutor();
            Callable<BigInteger> myPermutation1 = new Permutation(21000, 21000);
            Future<BigInteger> result1 = service1.submit(myPermutation1);
            
            ExecutorService service2 = Executors.newSingleThreadExecutor();
            Callable<BigInteger> myPermutation2 = new Permutation(14000, 21000);
            Future<BigInteger> result2 = service2.submit(myPermutation2);

            ExecutorService service3 = Executors.newSingleThreadExecutor();
            Callable<BigInteger> myPermutation3 = new Permutation(7000, 21000);
            Future<BigInteger> result3 = service3.submit(myPermutation3);
            
            finalResult = result1.get().multiply(finalResult);
            finalResult = result2.get().multiply(finalResult);
            finalResult = result3.get().multiply(finalResult);
            
            service1.shutdown();
            service2.shutdown();
            service3.shutdown();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return finalResult;        
    }

}
