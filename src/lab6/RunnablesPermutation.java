package lab6;

import java.math.BigInteger;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public class RunnablesPermutation {

    public static void main(String[] args) {
        BigInteger seqfact = BigInteger.ONE;
        BigInteger parfact = BigInteger.ONE;
        
        Permutation permtest = new Permutation(21000, 21000);
        
        // Start the clock
        long start = System.nanoTime();
        // Do sequential factorial
        for (int i = 1; i < 10; i++) {
            seqfact = permtest.call();
            System.out.print(".");
        }
        System.out.println();
        
        // Lap time
        long mid = System.nanoTime();
        // The parallel factorial
        for (int i = 1; i < 10; i++) {
            try {
                parfact = parfactorial();
                System.out.print(".");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        System.out.println();
        
        // Stop the clock
        long end = System.nanoTime();
        
        // Print out elapsed time
        System.out.printf("Single threaded took %.3f sec, multi-thread took %.3f%n", (mid-start) / 1e9, (end - mid) / 1e9);
        
        // Print out results
        System.out.println("seq factorial is " + seqfact);
        System.out.println("\n\npar factorial is " + parfact);
    }
    

    public static BigInteger parfactorial() throws ExecutionException, InterruptedException {
        ReturnObject threadResult1 = new ReturnObject();
        ReturnObject threadResult2 = new ReturnObject();
        ReturnObject threadResult3 = new ReturnObject();
        
        BigInteger finalResult = BigInteger.ONE;
        
        try {
            Thread t1 = new Thread(new PermutationR(7000, 7000, threadResult1));
            t1.start();
            
            Thread t2 = new Thread(new PermutationR(14000, 7000, threadResult1));
            t2.start();
            
            Thread t3 = new Thread(new PermutationR(21000, 7000, threadResult1));
            t3.start();
            
            // Wait for threads to complete
            t1.join();
            t2.join();
            t3.join();
            
            // Multiply results together
            finalResult = threadResult1.get().multiply(finalResult);
            finalResult = threadResult2.get().multiply(finalResult);
            finalResult = threadResult3.get().multiply(finalResult);
            
        } catch (Exception ex) {
            
        }
        return finalResult;
    }
    
    public static class Permutation implements Callable<BigInteger> {
        private long n;
        private long k;
        
        public Permutation(long n, long k) {
            this.n = n;
            this.k = k;
        }
        
        public BigInteger call() {
            BigInteger result = BigInteger.ONE;
            
            long a = n;
            long b = k;
            
            while (a >= b && b > 0) {
                result = result.multiply(BigInteger.valueOf(a));
                a--;
                b--;
            }
            
            return result;
        }
    }
    
    
    public static class PermutationR implements Runnable {
        private long n;
        private long k;
        
        public ReturnObject returnObject;
        
        public PermutationR(long n, long k, ReturnObject returnObject) {
            this.n = n;
            this.k = k;
            this.returnObject = returnObject;
        }
        
        public void run() {
            BigInteger result = BigInteger.ONE;
            
            long a = n;
            long b = k;
            
            while (a >= b && b > 0) {
                result = result.multiply(BigInteger.valueOf(a));
                a--;
                b--;
            }
            
            returnObject.set(result);
        }
    }
    
    public static class ReturnObject {
        private BigInteger value;
        
        public ReturnObject() {
            this.value = BigInteger.ONE;
        }
        
        public void set(BigInteger value) {
            this.value = value;
        }
        
        public BigInteger get() {
            return value;
        }
    }
    
}
