package lab6;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class LotsOfThreads {

    public static void main(String[] args) {
        BigInteger seqfact = BigInteger.ONE;
        BigInteger parfact = BigInteger.ONE;
        
        // Number of iterations
        int iter = 10;
        
        // Number of threads
        int threads = 1200;     // CHANGE THIS VALUE TO TEST PERFORMANCE
        
        // The factorial to calculate. Note: number / threads must be an exact integer
        int number = 24000;
        
        System.out.println("Number of threads that will be used is " + threads);
        
        int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("No. of processors available on this machine is " + processors);
        
        Permutation permtest = new Permutation(24000, 24000);
        
        // Start the clock
        long start = System.nanoTime();
        // Do sequential factorial
        for (int i = 1; i < iter; i++) {
            seqfact = permtest.call();
            System.out.print(".");
        }
        System.out.println();
        
        // Lap time
        long mid = System.nanoTime();
        // The parallel factorial
        for (int i = 1; i < iter; i++) {
            try {
                parfact = parfactorial(number, threads);
                System.out.print(".");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        System.out.println();
        
        // Stop the clock
        long end = System.nanoTime();
        
        // Print out elapsed time
        System.out.printf("Single threaded took %.3f sec, multi-thread took %.3f%n", (mid-start) / 1e9, (end - mid) / 1e9);
        System.out.printf("Speed up is %.1f times%n", ((mid-start)*1e0)/((end-mid)*1e0));
        
        // Print out results
        System.out.println("seq factorial is " + seqfact);
        System.out.println("\n\npar factorial is " + parfact);
    }
    

    public static BigInteger parfactorial(int n, int threads) throws ExecutionException, InterruptedException {
        BigInteger finalResult = BigInteger.ONE;
                
        // An array of jobs to be done by the threads
        ArrayList<Callable<BigInteger>> jobs = new ArrayList<Callable<BigInteger>>();
        
        // Array of futures, one for each thread
        ArrayList<Future<BigInteger>> results = new ArrayList<Future<BigInteger>>();
        
        // Array of results returned from the threads when they complete
        ArrayList<ExecutorService> services = new ArrayList<ExecutorService>();
        
        // Calculate the size r of each job (permutation
        // Eg. n = 21000, threads = 3, r = 7000
        int r = (n / threads);
        try {
            
            // Create threads to do work
            for ( int i = 0; i < threads; i++) {
                ExecutorService service = Executors.newSingleThreadExecutor();
                services.add(service);
                
                Callable<BigInteger> job = new Permutation((n-i*r), r);
                jobs.add(job);
                
                Future<BigInteger> result = service.submit(job);
                results.add(result);
            }
            
            // Get the results when threads are done and multiply together
            for( Future<BigInteger> result : results) {
                finalResult = result.get().multiply(finalResult);
            }
            
            // Clean up threads
            for (ExecutorService service : services) {
                service.shutdownNow();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return finalResult;        
    }
    

    public static class Permutation implements Callable<BigInteger> {
        private long n;
        private long k;
        
        public Permutation(long n, long k) {
            this.n = n;
            this.k = k;
        }
        
        public BigInteger call() {
            BigInteger result = BigInteger.ONE;
            
            long a = n;
            long b = k;
            
            while (a >= b && b > 0) {
                result = result.multiply(BigInteger.valueOf(a));
                a--;
                b--;
            }
            
            return result;
        }
    }
    
}
