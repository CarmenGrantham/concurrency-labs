package lab5;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello world from main");
        
        PetrolStationMonitor myPetrolStation = new PetrolStationMonitor();
        
        Thread t1 = new Thread(new Car(myPetrolStation));
        t1.start();
        Thread t2 = new Thread(new Car(myPetrolStation));
        t2.start();
        Thread t3 = new Thread(new Car(myPetrolStation));
        t3.start();
        Thread t4 = new Thread(new Car(myPetrolStation));
        t4.start();
        Thread t5 = new Thread(new Car(myPetrolStation));
        t5.start();
        Thread t6 = new Thread(new Car(myPetrolStation));
        t6.start();
        Thread t7 = new Thread(new Car(myPetrolStation));
        t7.start();
        Thread t8 = new Thread(new Car(myPetrolStation));
        t8.start();
        Thread t9 = new Thread(new Car(myPetrolStation));
        t9.start();
        Thread t10 = new Thread(new Car(myPetrolStation));
        t10.start();
    }
}
