package lab5;

public class PetrolStationMonitor {

    // Number of cars currently filling up
    private static int c = 0;
    
    // Temporary variables to ensure races are visible
    private static int temp1 = 0;
    private static int temp2 = 0;
    
    public synchronized void arrive() {
        try {        
            // When 4 pumps in use wait until one is free
            while (c > 3) {
                wait();
            }
        } catch (Exception e) {
            System.out.println("Exception " + e + " on wait.");
        }
        
        temp2 = c;
        delay();
        temp2 = temp2 + 1;
        delay();
        c = temp2;
        
        System.out.println("Monitor: cars filling incremented to " + c);
    }
    
    public synchronized void depart() {
        temp1 = c;
        delay();
        temp1 = temp1 - 1;
        delay();
        c = temp1;
        System.out.println("Monitor: cars filling decremented to " + c);

        try {
            notifyAll();
        } catch (Exception e) {
            System.out.println("Exception " + e + " on notifyAll.");
        }
    }
    
    public int carsfilling() {
        return c;
    }

    public static void delay() {
        int actualDelay;
        
        try {
            // thread to sleep for random milliseconds
            actualDelay = randomWithRange(0, 1000);
            Thread.sleep(actualDelay);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    private static int randomWithRange(int min, int max) {
        int range = (max-min) + 1;
        return (int) (Math.random() * range) + min;
    }
}
