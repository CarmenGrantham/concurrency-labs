package lab5;

public class Car implements Runnable {

    private static PetrolStationMonitor myMonitor;
    
    public Car(PetrolStationMonitor m) {
        this.myMonitor = m;
    }
    
    public void run() {
        long threadId = Thread.currentThread().getId();
        System.out.println("Starting car " + threadId);
        delay();
        
        System.out.println("Car " + threadId + ": trying to arrive");
        myMonitor.arrive();
        
        System.out.println("Car " + threadId + ": filling. Total number of cars filling = " + myMonitor.carsfilling());
        delay();
        
        System.out.println("Car " + threadId + ": trying to depart");
        myMonitor.depart();
        
        System.out.println("Car " + threadId + ": has departed. Total number of cars filling= " + myMonitor.carsfilling());
    }
    

    public static void delay() {
        int actualDelay;
        
        try {
            // thread to sleep for random milliseconds
            actualDelay = randomWithRange(0, 1000);
            Thread.sleep(actualDelay);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    private static int randomWithRange(int min, int max) {
        int range = (max-min) + 1;
        return (int) (Math.random() * range) + min;
    }
}
