package lab3;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;


// Class that implements the work of each thread
public class ImageProcessorRunnable implements Runnable {

    private BufferedImage myImage;
    private int myStartWidth;
    private int mySize;
    private int myHeight;

    // Constructor
    // The thread is given the image and the starting position for the processing
    // and the size of the vertical strip to process
    public ImageProcessorRunnable(BufferedImage image, int height, int startWidth, int size) throws Exception {
        myImage = image;
        myStartWidth = startWidth;
        mySize = size;
        myHeight = height;
    }

    public void run() {
        // Insert an artifical delay here to demonstrate the effect of not waiting for the thread to finish
        try {
            Thread.sleep(0);
            //Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //The work here is to convert the colour pixels to grayscale
        // For an explanation of what is happening see
        // https://www.dyclassroom.com/image-processing-project/how-to-convert-a-color-image-into-grayscale-image-in-java
        // First loop over the y axis pixels
        for (int y = 0; y < myHeight; y++) {
            // Now loop over the x axis pixels that have been allocated to this thread
            for (int x = myStartWidth; x < myStartWidth + mySize; x++) {
                // fetch the colour pixel
                int p = myImage.getRGB(x, y);
                // extract the different colours
                int a = (p >> 24) & 0xff;
                int r = (p >> 16) & 0xff;
                int g = (p >> 8) & 0xff;
                int b = p & 0xff;
                //calculate average of the colours
                int avg = (r + g + b) / 3;
                //replace  the RGB colour value with the average
                p = (a << 24) | (avg << 16) | (avg << 8) | avg;
                // write the pixel back to the image
                myImage.setRGB(x, y, p);
            }
        }
        //System.out.println("imageRunnable is finished");
    }
}
