package lab3;

public class MySecondRunnable implements Runnable {

    @Override
    public void run() {
        try {
            Thread.currentThread().sleep(1);            
        } catch (Exception e) {
        }
        System.out.printf("I'm running in thread %s \n", Thread.currentThread().getName());

    }

}
