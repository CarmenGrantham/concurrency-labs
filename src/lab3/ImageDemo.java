package lab3;

import java.awt.*;
import java.awt.image.*;
import javax.swing.*;

/**
 * A Java class to demonstrate how to load an image from disk with the
 * ImageIO class. Also shows how to display the image by creating an
 * ImageIcon, placing that icon an a JLabel, and placing that label on
 * a JFrame.
 *
 * @author alvin alexander, devdaily.com
 * Commenst added by D Kearney
 */
public class ImageDemo
{
    public static void main(String[] args) throws Exception
    {
        // set the name of the image file here
        new ImageDemo("cat.jpg");
    }

    public ImageDemo(final String filename) throws Exception
            // All the processing is done in this constructor
    {
        // Combining threading with the swing GUI requires invokeLater
        // An advanced topic not covered in this lab
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run() {

                // Create an variable to store the image when it comes back from processing
                BufferedImage image=null;
                // Because ImageProcessor uses threads it can throw exceptions that must be caught
                try {
                    // Load the image file to be processed
                    ImageProcessor myprocessor = new ImageProcessor(filename);
                    // Process the image and return the result
                    image = ImageProcessor.StartProcessing();
                } catch (Exception e) {
                        e.printStackTrace();
                    }

                // Code to display the image using Swing
                JFrame editorFrame = new JFrame("Image Demo");
                editorFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                ImageIcon imageIcon = new ImageIcon(image);
                JLabel jLabel = new JLabel();
                jLabel.setIcon(imageIcon);
                editorFrame.getContentPane().add(jLabel, BorderLayout.CENTER);
                editorFrame.pack();
                editorFrame.setLocationRelativeTo(null);
                editorFrame.setVisible(true);
            }
        });
    }
}