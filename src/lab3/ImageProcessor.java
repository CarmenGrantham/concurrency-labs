package lab3;

import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

// Class to setup and run threads to process an image
public class  ImageProcessor {

    // Local copy of the image
    static BufferedImage image = null;
    // width of the image in pixels
    static int width = 0;
    // height of the image in pixels
    static int height = 0;

    public ImageProcessor(String filename) throws Exception {
        String myFilename = filename;
        // Read the image from file
        image = ImageIO.read(new File(myFilename));
        //get image width and height
        width = image.getWidth();
        height = image.getHeight();
        System.out.println("Image from file "+myFilename+" width= " + width + " height= " + height);

    }

    // Method to create and start threads - returns processed image
    public static BufferedImage StartProcessing() throws Exception {

        // Create new runnable instances on which threads will be based
        // It is assumed that the image is 1600 pixels wide at least
        // Each thread is given a vertical strip of the image 200 pixels wide to process
        ImageProcessorRunnable myimageRunnable1 = new ImageProcessorRunnable(image, height, 0, 200);
        ImageProcessorRunnable myimageRunnable2 = new ImageProcessorRunnable(image, height, 400, 200);
        ImageProcessorRunnable myimageRunnable3 = new ImageProcessorRunnable(image, height, 800, 200);
        ImageProcessorRunnable myimageRunnable4 = new ImageProcessorRunnable(image, height, 1200, 200);

        /*
        myimageRunnable1.run();
        myimageRunnable2.run();
        myimageRunnable3.run();
        myimageRunnable4.run();
        */
        
        for (int i = 0; i < 100; i++) {
            
            Thread t1 = new Thread(myimageRunnable1);
            Thread t2 = new Thread(myimageRunnable2);
            Thread t3 = new Thread(myimageRunnable3);
            Thread t4 = new Thread(myimageRunnable4);
            
            t1.start();
            t2.start();
            t3.start();
            t4.start();
    
            // Wait until all threads have completed execution.
            t1.join();
            t2.join();
            t3.join();
            t4.join();
        }        




        return image;
    }
}
