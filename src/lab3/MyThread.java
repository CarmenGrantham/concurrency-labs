package lab3;

public class MyThread implements Runnable {
    

    public MyThread() {
        
    }

    @Override
    public void run() {
        int t1 = 1;
        int t2 = 10;
        for (int count = 1; count < 10; count++)  {
            System.out.println("MyThread is running (t1 = " + t1 + ", t2 = " + t2 +")");
            delay(t1, t2);
        }
        
    }
    
    public static void delay(int min, int max) {
        int actualDelay;
        
        try {
            // thread to sleep for random milliseconds
            actualDelay = randomWithRange(min, max);
            Thread.sleep(actualDelay);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    private static int randomWithRange(int min, int max) {
        int range = (max-min) + 1;
        return (int) (Math.random() * range) + min;
    }
    
    
}
