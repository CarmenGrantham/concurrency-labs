package lab3;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        long t3 = 1;
        long t4 = 5;
        
        Thread p1 = new Thread(new MyThread(), "p1");
        p1.start();
        Thread.sleep(t3);
        for (int j = 0; j < 10; j++) {
            System.out.println("Main class is running (t3 = " + t3 + ", t4 = " + t4 +")");
            Thread.sleep(t4);
        }
        
        // Start 12 threads using MySecondRunnable
        for (int i = 0; i < 12; i++) {
            Thread t = new Thread(new MySecondRunnable());
            t.setName("secondThread_" + i);
            t.start();
        }
    }
}
