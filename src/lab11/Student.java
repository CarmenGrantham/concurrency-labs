package lab11;

public class Student implements Runnable {
    private BookingController mybc;
                    
    private String mybp;

    public Student(BookingController b,String bookingPattern) {
        this.mybc = b;
        this.mybp = bookingPattern;

    }

    public void run() {
        long threadId = Thread.currentThread().getId();
        System.out.println("Starting student "+threadId+" booking pattern "+mybp);
        delay();
        if (mybp == "P") mybc.P();
        if (mybp == "Q") mybc.Q();
    }


    public static void delay() {
        int actualDelay;
        
        try {
            // thread to sleep for random milliseconds
            actualDelay = randomWithRange(0, 1000);
            Thread.sleep(actualDelay);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    private static int randomWithRange(int min, int max) {
        int range = (max-min) + 1;
        return (int) (Math.random() * range) + min;
    }

}
