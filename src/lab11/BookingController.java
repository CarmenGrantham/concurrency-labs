package lab11;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class BookingController {
    // semaphores for trips
    Semaphore lm9 = new Semaphore(1);
    Semaphore cse10 = new Semaphore(1);
    // the list of booked trips
    ArrayList<String> bookedTrips = new ArrayList<String>();


    //Booking pattern P

    //public synchronized void P() {
    public void P() {
        System.out.println("P starting");
        try {
            lm9.acquire();
            System.out.println("lm9 acquired by student "+ Thread.currentThread().getId());
            longdelay();
            cse10.acquire();
            System.out.println("cse10 acquired by student "+ Thread.currentThread().getId());
            delay();
            if (isbooked("lm9") || isbooked("cse10"))
            System.out.println("bookingController :no booking made one of the trips is taken ");
            else {
                book("lm9");
                book("cse10");
            }

            cse10.release();
            System.out.println("cse10 released");
            delay();
            lm9.release();
            System.out.println("lm9 released");
        } catch (Exception e) {}

    }


    //Booking pattern Q
    
    //public synchronized void Q() {
    public void Q() {
        System.out.println("Q starting");
        try {
            cse10.acquire();
            System.out.println("cse10 acquired by student "+ Thread.currentThread().getId());
            longdelay();
            lm9.acquire();
            System.out.println("lm9 acquired by student "+ Thread.currentThread().getId());
            delay();
            if (isbooked("lm9") || isbooked("cse10"))
                System.out.println("bookingController :no booking made one of the trips is taken ");
            else {
                book("lm9");
                book("cse10");
            }

            cse10.release();
            System.out.println("cse10 released");
            delay();
            lm9.release();
            System.out.println("lm9 released");
        } catch (Exception e) {}
    }

    
    /*
     
    public void P() {
        System.out.println("P starting");
        try {
            synchronized(this) {
                lm9.acquire();
                System.out.println("lm9 acquired by student "+ Thread.currentThread().getId());
                longdelay();
                cse10.acquire();
                System.out.println("cse10 acquired by student "+ Thread.currentThread().getId());
                delay();
                if (isbooked("lm9") || isbooked("cse10"))
                System.out.println("bookingController :no booking made one of the trips is taken ");
                else {
                    book("lm9");
                    book("cse10");
                }
    
                cse10.release();
                System.out.println("cse10 released");
                delay();
                lm9.release();
            }
            System.out.println("lm9 released");
        } catch (Exception e) {}

    }

    public void Q() {
        System.out.println("Q starting");
        try {
            synchronized(this) {
                cse10.acquire();
                System.out.println("cse10 acquired by student "+ Thread.currentThread().getId());
                longdelay();
                lm9.acquire();
                System.out.println("lm9 acquired by student "+ Thread.currentThread().getId());
                delay();
                if (isbooked("lm9") || isbooked("cse10"))
                    System.out.println("bookingController :no booking made one of the trips is taken ");
                else {
                    book("lm9");
                    book("cse10");
                }
    
                cse10.release();
                System.out.println("cse10 released");
                delay();
                lm9.release();
            }
            System.out.println("lm9 released");
        } catch (Exception e) {}
    }

     */
    
    
    // add a booked trip to the list if it is not already booked return true
    
    public  void book(String trip) {
        if (bookedTrips.contains(trip)) {
            System.out.println("Trip "+trip+" already booked");
        } else {
            bookedTrips.add(trip);
            System.out.println("Booking " + trip+ " booked trips are "+bookedTrips);
        }
        return;
    }

     public  boolean isbooked(String trip) {
         if (bookedTrips.contains(trip)) {
             System.out.println("Trip "+trip+" already booked");
             return true;
         }
         else return false;
     }

    public static void delay() {
        int actualDelay;
        
        try {
            // thread to sleep for random milliseconds
            actualDelay = randomWithRange(0, 1000);
            Thread.sleep(actualDelay);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    private static int randomWithRange(int min, int max) {
        int range = (max-min) + 1;
        return (int) (Math.random() * range) + min;
    }

    public static void longdelay() {
        try {
            // Sleep for 3 seconds
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
