package lab11;

public class Main {
    public static void main(String[] args) {
        // write your code here
        System.out.println("Hello world from main");

        BookingController mybookingController = new BookingController();
        
        /* Question 1 - All P
        Thread t1 = new Thread(new Student(mybookingController,"P"));
        t1.start();
        Thread t2 = new Thread(new Student(mybookingController,"P"));
        t2.start();
        Thread t3 = new Thread(new Student(mybookingController,"P"));
        t3.start();
        Thread t4 = new Thread(new Student(mybookingController,"P"));
        t4.start();
        */
         
        /* Question 2 - P then Q with long delay and P() and Q() synchronized */ 
        Thread t1 = new Thread(new Student(mybookingController,"P"));
        t1.start();
        Thread t2 = new Thread(new Student(mybookingController,"Q"));
        t2.start();
         
        
        /*
        Thread t2 = new Thread(new Student(mybookingController,"Q"));
        t2.start();
        Thread t3 = new Thread(new Student(mybookingController,"Q"));
        t3.start();
        Thread t4 = new Thread(new Student(mybookingController,"Q"));
        t4.start();
         */

        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        try {
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        System.out.println("Finished.");
    }

}
