package lab2;

enum action {a0,a1,a2};
enum State {S0,S1,S2};


public class Main {
    public static void main(String[] args) {
        System.out.println("Starting main");
        // Initialize a new state machine to state S0
        StateMachine mystatemachine = new StateMachine(State.S0);
        // perform action a0 to move to S1
        mystatemachine.changeState(action.a0);
        // perform action a1 to move to S2
        mystatemachine.changeState(action.a1);
        // perform action a2 to move to S0
        mystatemachine.changeState(action.a2);
        // perform action a0 to move to S1
        mystatemachine.changeState(action.a0);
        // performing action a2 in state S1 is not permitted
        mystatemachine.changeState(action.a2);
    }
}
