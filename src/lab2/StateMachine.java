package lab2;

public class StateMachine {
    static State myState;

    public StateMachine(State initialState) {
        myState = initialState;
    }


    static void changeState(action act) {


        switch (myState) {

            case S0:
                if (act == action.a0) {
                    myState = State.S1;
                    System.out.println("Moved to state "+myState);
                } else {
                	System.out.println("action " + act + " not permitted in state " + myState);
                }
                break;
            case S1:
                if (act == action.a1) {
                    myState = State.S2;
                    System.out.println("Moved to state "+myState);
                } else {
                	System.out.println("action " + act + " not permitted in state " + myState);
                }
                break;
            case S2:
                if (act == action.a2) {
                    myState = State.S0;
                    System.out.println("Moved to state "+myState);
                } else {
                	System.out.println("action " + act + " not permitted in state " + myState);
                }
                break;
        }
    }
}
