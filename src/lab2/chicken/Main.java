package lab2.chicken;

enum action {hatch, grow, lay, cook};
enum State {CHICK, CHICKEN, EGG, OMELET};


public class Main {
    public static void main(String[] args) {
        System.out.println("Starting main chicken");
        // Initialize a new state machine to state S0
        StateMachine mystatemachine = new StateMachine(State.EGG);
        // perform action hatch to move to CHICK
        mystatemachine.changeState(action.hatch);
        // perform action grow to move to CHICKEN
        mystatemachine.changeState(action.grow);
        // perform action lay to move to EGG
        mystatemachine.changeState(action.lay);
        // perform action cook to move to OMELET
        mystatemachine.changeState(action.cook);
    }
}
