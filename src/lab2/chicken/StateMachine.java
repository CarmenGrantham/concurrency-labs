package lab2.chicken;

public class StateMachine {
    static State myState;

    public StateMachine(State initialState) {
        myState = initialState;
    }


    static void changeState(action act) {


        switch (myState) {

            case CHICK:
                if (act == action.grow) {
                    myState = State.CHICKEN;
                    System.out.println("Moved to state "+myState);
                } else {
                	System.out.println("action " + act + " not permitted in state " + myState);
                }
                break;
            case CHICKEN:
                if (act == action.lay) {
                    myState = State.EGG;
                    System.out.println("Moved to state "+myState);
                } else {
                	System.out.println("action " + act + " not permitted in state " + myState);
                }
                break;
            case EGG:
                if (act == action.cook) {
                    myState = State.OMELET;
                    System.out.println("Moved to state "+myState);
                } else if (act == action.hatch){ 
                	myState = State.CHICK;
                    System.out.println("Moved to state "+myState);
                } else {
                	System.out.println("action " + act + " not permitted in state " + myState);
                }
                break;
            case OMELET:
              	System.out.println("action " + act + " not permitted in state " + myState);
                break;
        }
    }
}
