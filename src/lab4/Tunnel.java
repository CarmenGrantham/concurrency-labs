package lab4;

public class Tunnel {
    
    private boolean occupied = false;
    
    public void enter() {
        if (occupied) {
            System.out.println("Error! train has entered a tunnel that is already occupied");
        }
        occupied = true;
        System.out.println("A train has entered the tunnel");
    }
    
    public void exit() {
        if (!occupied) {
            System.out.println("Error! train has exited a tunnel that is not occupied");
        }
        occupied = false;
        System.out.println("A train has exited the tunnel");
    }
    
    
    public static void main(String[] args) {
        System.out.println("Expected behaviour....");
        Tunnel myTunnel1 = new Tunnel();
        myTunnel1.enter();
        myTunnel1.exit();
        
        System.out.println("\nCalling exit before enter");
        Tunnel myTunnel2 = new Tunnel();
        myTunnel2.exit();
        
        System.out.println("\nCalling enter twice in a row");
        Tunnel myTunnel3 = new Tunnel();
        myTunnel3.enter();
        myTunnel3.enter();
    }
}
