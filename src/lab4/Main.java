package lab4;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello world from main");
        
        TunnelWithSemaphore myTunnelWithSemaphore = new TunnelWithSemaphore();
        Tunnel myTunnel = new Tunnel();
        boolean usingSemaphore = true;
        
        if (usingSemaphore) {
            System.out.println("Using a semaphore");
            Thread train1 = new Thread(new TrainWithSemaphore(myTunnelWithSemaphore));
            train1.start();
            Thread train2 = new Thread(new TrainWithSemaphore(myTunnelWithSemaphore));
            train2.start();
        } else {
            System.out.println("NOT using a semphore");
            Thread train1 = new Thread(new Train(myTunnel));
            train1.start();
            Thread train2 = new Thread(new Train(myTunnel));
            train2.start();
        }
        
    }
}
