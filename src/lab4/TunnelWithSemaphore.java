package lab4;

import java.util.concurrent.Semaphore;

public class TunnelWithSemaphore {
    private Semaphore tunnelSemaphore = new Semaphore(1);

    private boolean occupied = false;

    public void enter() {
        try {
            tunnelSemaphore.acquire();
            if (occupied) {
                System.out.println("Error! train has entered a tunnel that is already occupied");
            }
            occupied = true;
            System.out.println("A train has entered the tunnel");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    public void exit() {
        if (!occupied) {
            System.out.println("Error! train has exited a tunnel that is not occupied");
        }
        occupied = false;
        System.out.println("A train has exited the tunnel");

        tunnelSemaphore.release();
    }
    
}
