package lab4;

public class Train implements Runnable {

    private Tunnel tunnel;
    
    public Train(Tunnel tunnel) {
        this.tunnel = tunnel;
    }
    
    @Override
    public void run() {
        System.out.printf("Staring Train %s\n", Thread.currentThread().getId());
        
        System.out.printf("Train %s TRYING to enter tunnel\n", Thread.currentThread().getId());
        tunnel.enter();
        System.out.printf("Train %s has entered the tunnel\n", Thread.currentThread().getId());
        
        delay();
        
        System.out.printf("Train %s TRYING to exit tunnel\n", Thread.currentThread().getId());
        tunnel.exit();
        System.out.printf("Train %s has exited the tunnel\n", Thread.currentThread().getId());
        
    }


    public static void delay() {
        int actualDelay;
        
        try {
            // thread to sleep for random milliseconds
            actualDelay = randomWithRange(0, 1000);
            Thread.sleep(actualDelay);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    private static int randomWithRange(int min, int max) {
        int range = (max-min) + 1;
        return (int) (Math.random() * range) + min;
    }
    
    
}
